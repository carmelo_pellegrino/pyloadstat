#!/usr/bin/env python

# This is the port bound by the UDP server of ROyWeb
udp_port = 9999

# This is the number of seconds between one send and another
refresh_period = 2

# This is the maximum number of samples to consider in the average
nsamples = 5

# Name choosen for historical reasons
class loadStats:
  def __init__(self, iface):
    self.iface = iface
    self.previous = self.load()

  def load(self):
    f = open("/proc/net/dev", "r");
    data = f.read()
    f.close()

    fields = data[data.find(self.iface) + len(self.iface) + 2:].split()

    return (int(fields[0]), int(fields[8]))

  def get(self):
    current = self.load()
    data = ((current[0] - self.previous[0]) / refresh_period,
        (current[1] - self.previous[1]) / refresh_period)

    self.previous = current

    return data

if __name__ == "__main__":
  import json
  import socket
  from time import sleep, time
  import argparse
  parser = argparse.ArgumentParser(
    description='Sends throughput values to ROyWeb server.')

  parser.add_argument('--basetag', '-b',
    metavar='tag',
    dest = 'base_tag',
    default = socket.gethostname(),
    help = 'Base string for both input and output tags (default: %s).'
      % socket.gethostname())

  parser.add_argument('--interface', '-i',
    metavar='interface',
    dest = 'iface',
    default = 'eth0',
    help = 'Network interface to monitor (default: eth0).')

  parser.add_argument('--address', '-a',
    metavar='address',
    dest = 'server_address',
    default = '127.0.0.1',
    help = 'IP Address of the ROyWeb UDP server (default: localhost).')

  args = parser.parse_args()

  base_tag = args.base_tag
  iface = args.iface
  input_tag = base_tag + '_' + iface + '_in'
  output_tag = base_tag + '_' + iface + '_out'
  server_address = args.server_address

  print("""Program setup:

Interface: %s
Output tag: %s
Input tag: %s
ROyWeb server address: %s"""
% (iface, output_tag, input_tag, server_address))

  try:
    source = loadStats(iface)
  except:
    print("Error reading the interface (%s) data" % iface)
    exit(-1)

  in_buf = []
  in_sum = 0
  out_buf = []
  out_sum = 0

  while True:
    start_time = time()
    throughput = source.get()

    print(throughput)

    in_sum  = in_sum + throughput[0]
    in_buf.append(throughput[0])
    out_sum = out_sum + throughput[1]
    out_buf.append(throughput[1])

    if len(in_buf) >= nsamples:
      in_sum  = in_sum - in_buf.pop(0)
      out_sum = out_sum - out_buf.pop(0)

    message = json.dumps({'kind': 'parameter',
         'type': input_tag,
         'unit': 'Bps',
         'description': 'Total incoming throughput on interface ' + iface,
         'value': throughput[0]
          })

    socket.socket(socket.AF_INET, socket.SOCK_DGRAM).sendto(
        message,
        (server_address, udp_port))

    message = json.dumps({'kind': 'parameter',
         'type': output_tag,
         'unit': 'Bps',
         'description': 'Total outcoming throughput on interface ' + iface,
         'value': throughput[1]
          })

    socket.socket(socket.AF_INET, socket.SOCK_DGRAM).sendto(
        message,
        (server_address, udp_port))

    sleep_time = refresh_period + start_time - time()
    if sleep_time > 0:
      sleep(sleep_time)
